package com.mozzartbet.mobileagent.ui.vip

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mozzartbet.mobileagent.R
import kotlinx.android.synthetic.main.activity_transaction_details.*
import org.w3c.dom.Text
import java.lang.Exception
import java.util.*

class VIPActivity : AppCompatActivity() {

    private lateinit var vipViewModel: VIPViewModel
    private lateinit var vipContent: View
    private lateinit var userInfo: EditText
    private lateinit var amount: EditText
    private lateinit var submit: Button
    private lateinit var randomUUID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vip)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        vipViewModel =
            ViewModelProviders.of(this, VIPViewModelFactory())
                .get(VIPViewModel::class.java)
        vipContent = findViewById(R.id.content_vip)
        userInfo = findViewById(R.id.user_info)
        amount = findViewById(R.id.amount)
        submit = findViewById(R.id.submit)

        userInfo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                randomUUID = UUID.randomUUID().toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        submit.setOnClickListener {
            try {
                if (TextUtils.isEmpty(userInfo.text.toString()) || TextUtils.isEmpty(amount.text.toString())) {
                    Toast.makeText(this, R.string.fields_cannot_be_empty, Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                submit.isEnabled = false
                vipViewModel.createTransaction(
                    userInfo.text.toString(),
                    amount.text.toString().toDouble()
                )
            } catch (e: Exception) {
                submit.isEnabled = true
                Snackbar.make(vipContent, e.localizedMessage, Snackbar.LENGTH_SHORT).show()
            }
        }

        vipViewModel.vipFormState.observe(this, Observer {

            submit.isEnabled = true
            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        vipContent,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.message)) {
                it.message?.let { it1 ->
                    Toast.makeText(
                        this,
                        it1,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            if (it.transaction != null) {
                randomUUID = UUID.randomUUID().toString()
                AlertDialog.Builder(this)
                    .setTitle(R.string.check_data)
                    .setMessage(it.transaction.showUserDetails(this))
                    .setPositiveButton(
                        R.string.handle_tranaction
                    ) { _, _ -> vipViewModel.handleTransaction(it.transaction) }
                    .setNegativeButton(
                        R.string.cancel
                    ) { _, _ -> vipViewModel.unassignTransaction(it.transaction.id) }
                    .setCancelable(false)
                    .show()
            }
            if (!TextUtils.isEmpty(it.handleMessage)) {
                if ("SUCCESS" == it.handleMessage) {
                    finish()
                } else {
                    Toast.makeText(
                        this,
                        it.handleMessage,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
