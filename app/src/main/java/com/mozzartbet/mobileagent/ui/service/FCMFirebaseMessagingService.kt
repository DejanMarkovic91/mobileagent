package com.mozzartbet.mobileagent.ui.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import com.mozzartbet.mobileagent.ui.login.LoginActivity
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsActivity


@Suppress("DEPRECATION")
class FCMFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val mBuilder = NotificationCompat.Builder(applicationContext, "global")
        if(remoteMessage.data.containsKey("transaction")){
            //open transaction details
            val ii = Intent(applicationContext, TransactionDetailsActivity::class.java)
            val bundle = Bundle()
            bundle.putString("TRANSACTION", remoteMessage.data["transaction"] as String)
            ii.putExtras(bundle)
            val pendingIntent = PendingIntent.getActivity(this, 0, ii, 0)
            mBuilder.setContentIntent(pendingIntent)
        }else {
            //open app
            val ii = Intent(applicationContext, LoginActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, ii, 0)
            mBuilder.setContentIntent(pendingIntent)
        }
        mBuilder.setSmallIcon(R.drawable.ic_stat_agent)
        mBuilder.setContentTitle(remoteMessage.data["title"])
        mBuilder.setContentText(remoteMessage.data["message"])
        mBuilder.setAutoCancel(true)
        mBuilder.priority = Notification.PRIORITY_MAX

        var mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "global"
            val channel = NotificationChannel(
                channelId,
                "global",
                NotificationManager.IMPORTANCE_HIGH
            )
            mNotificationManager.createNotificationChannel(channel)
            mBuilder.setChannelId(channelId)
        }

        mNotificationManager.notify((Math.random() * 1000).toInt(), mBuilder.build())
    }
}