package com.mozzartbet.mobileagent.ui.personal

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mozzartbet.mobileagent.data.LoginDataSource
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainDataSource
import com.mozzartbet.mobileagent.data.MainRepository

class MyTransactionsViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MyTransactionsViewModel::class.java)) {
            return MyTransactionsViewModel(
                mainRepository = MainRepository(
                    dataSource = MainDataSource()
                ),
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}