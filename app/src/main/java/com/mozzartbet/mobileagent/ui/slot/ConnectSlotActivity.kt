package com.mozzartbet.mobileagent.ui.slot

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.mozzartbet.mobileagent.R
import kotlinx.android.synthetic.main.activity_connect_slot.*

class ConnectSlotActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connect_slot)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.action_connect_slot)
        submit.setOnClickListener {

        }
    }
}