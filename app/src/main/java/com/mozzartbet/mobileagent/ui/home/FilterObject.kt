package com.mozzartbet.mobileagent.ui.home

import com.mozzartbet.mobileagent.ui.home.FilterConstants.PAYIN

object FilterConstants {
    const val PAYIN = 1
    const val PAYOUT = 2
}

class FilterObject(var searchQuery: String = "", var type: Int = PAYIN)