package com.mozzartbet.mobileagent.ui.slot

data class SlotConnectFormState (
    val error: String? = null,
    val message: String? = null
)