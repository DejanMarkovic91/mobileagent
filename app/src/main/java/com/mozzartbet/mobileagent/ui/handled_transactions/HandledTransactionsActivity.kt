package com.mozzartbet.mobileagent.ui.handled_transactions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.mozzartbet.mobileagent.MobileAgentApplication
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.ui.adapter.HomeContentAdapter
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.HandledTransactionItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsActivity
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsViewModel
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsViewModelFactory
import com.mozzartbet.mobileagent.ui.user_details.UserDetailsViewModel
import com.mozzartbet.mobileagent.ui.user_details.UserDetailsViewModelFactory
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.HorizontalCalendarView
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import kotlinx.android.synthetic.main.activity_transaction_details.*
import java.util.*

class HandledTransactionsActivity : AppCompatActivity(), HomeContentAdapter.OnItemClickListener {

    private var lastSelected: Calendar = Calendar.getInstance()
    private lateinit var transactionsViewModel: TransactionDetailsViewModel
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var adapter: HomeContentAdapter
    private lateinit var contentList: RecyclerView
    private lateinit var calendarView: HorizontalCalendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_handled_transactions)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        contentList = findViewById(R.id.content)
        contentList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = HomeContentAdapter()
        adapter.setOnItemClickListener(this)
        contentList.adapter = adapter
        contentList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        val old = Calendar.getInstance(getLocale())
        old.add(Calendar.DAY_OF_MONTH, -5);
        calendarView = HorizontalCalendar.Builder(this, R.id.calendarView)
            .range(old, Calendar.getInstance(getLocale()))
            .datesNumberOnScreen(5)
            .build()
        calendarView.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar?, position: Int) {
                date?.let {
                    swipeRefresh.isRefreshing = true
                    lastSelected = date
                    transactionsViewModel.loadHandledTransactionsForDate(it)
                }
            }
        }

        transactionsViewModel = ViewModelProviders.of(this, TransactionDetailsViewModelFactory())
            .get(TransactionDetailsViewModel::class.java)

        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            transactionsViewModel.loadHandledTransactionsForDate(lastSelected)
        }
        swipeRefresh.isRefreshing = true
        transactionsViewModel.loadHandledTransactionsForDate(Calendar.getInstance(getLocale()))

        transactionsViewModel.transactionDetailsFormState.observe(this@HandledTransactionsActivity, androidx.lifecycle.Observer {
            swipeRefresh.isRefreshing = false
            adapter.items = it.handledItems!!
            adapter.notifyDataSetChanged()

            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.message)) {
                it.message?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun getLocale(): Locale {
        return (applicationContext as MobileAgentApplication).locale
    }

    override fun onClick(position: Int, item: ContentItem) {
        val intent = Intent(this, TransactionDetailsActivity::class.java)
        intent.putExtra("TRANSACTION", (item as HandledTransactionItem).moneyDocument)
        startActivity(intent)
    }
}
