package com.mozzartbet.mobileagent.ui.login

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.messaging.FirebaseMessaging
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.model.LoggedInUser
import com.mozzartbet.mobileagent.helper.CredentialStorage

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String, rememberMe: Boolean, context: Context) {
        // can be launched in a separate asynchronous job
        loginRepository.login(username, password)!!.subscribe({ jwt ->
            run {
                com.mozzartbet.mobileagent.data.user = LoggedInUser(username, jwt)
                _loginResult.value =
                    LoginResult(success = LoggedInUserView(displayName = com.mozzartbet.mobileagent.data.user!!.displayName))
                val credentialsHelper = CredentialStorage(context)
                if (rememberMe)
                    credentialsHelper.storeCredentials(username, password)
                else
                    credentialsHelper.storeCredentials("","")
                FirebaseMessaging.getInstance()
                    .subscribeToTopic(com.mozzartbet.mobileagent.data.user!!.displayName)
                    .addOnCompleteListener {}
            }
        }, { _ ->
            run {
                _loginResult.value = LoginResult(error = R.string.login_failed)
            }
        })
    }

    fun getStoredCredentials(context: Context): Array<out String>? {
        val credentialsHelper = CredentialStorage(context)
        return credentialsHelper.readCredentials()
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.isNotEmpty()
    }
}
