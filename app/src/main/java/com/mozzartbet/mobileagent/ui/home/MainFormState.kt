package com.mozzartbet.mobileagent.ui.home

import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import java.util.ArrayList

data class MainFormState(
    val items: List<ContentItem> = ArrayList(),
    val error: String? = null,
    val updateURL: String? = null,
    val message: String? = null
)