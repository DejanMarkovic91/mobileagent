package com.mozzartbet.mobileagent.ui.user_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainRepository
import com.mozzartbet.mobileagent.data.user

class UserDetailsViewModel(
    private val mainRepository: MainRepository,
    private val loginRepository: LoginRepository
) : ViewModel() {
    private val _userDetailsForm = MutableLiveData<UserDetailsFormState>()
    val userDetailsFormState: LiveData<UserDetailsFormState> = _userDetailsForm

    fun loadData(inputField: String) {
        mainRepository.getUserDetails(
            user!!.displayName,
            inputField
        )!!.subscribe({ items ->
            run {
                _userDetailsForm.value =
                    UserDetailsFormState(
                        items = items
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _userDetailsForm.value = UserDetailsFormState(
                error = throwable.localizedMessage
            )
        })
    }

}