package com.mozzartbet.mobileagent.ui.slot

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mozzartbet.mobileagent.data.*

class SlotConnectModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SlotConnectViewModel::class.java)) {
            return SlotConnectViewModel(
                repository = SlotConnectRepository(
                    dataSource = MainDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}