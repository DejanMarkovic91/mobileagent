package com.mozzartbet.mobileagent.ui.personal

import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import java.util.ArrayList

class MyTransactionsFormState(
    val items: List<ContentItem> = ArrayList(),
    val error: String? = null,
    val message: String? = null
)