package com.mozzartbet.mobileagent.ui.adapter.item

import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import java.lang.Exception

class HandledTransactionItem (val moneyDocument: MoneyDocument) :
    ContentItem(ContentConstants.HANDLED_TRANSACTION_ITEM) {
    fun contains(query: String): Boolean {
        return try {
            moneyDocument.id == query.toLong()
        } catch (e: Exception) {
            false
        }
//        return moneyDocument.address.toLowerCase().contains(query) ||
//                moneyDocument.mobileNumber.contains(query) ||
//                moneyDocument.player!!.username.contains(query) ||
//                moneyDocument.player.firstname.contains(query) ||
//                moneyDocument.player.lastname.contains(query)
    }
}