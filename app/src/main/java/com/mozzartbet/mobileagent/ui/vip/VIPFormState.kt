package com.mozzartbet.mobileagent.ui.vip

import com.mozzartbet.mobileagent.network.dto.MoneyDocument

data class VIPFormState(
    val error: String? = null,
    val message: String? = null,
    val transaction: MoneyDocument? = null,
    val handleMessage: String? = null
)