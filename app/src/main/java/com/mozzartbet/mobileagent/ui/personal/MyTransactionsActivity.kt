package com.mozzartbet.mobileagent.ui.personal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.ui.adapter.HomeContentAdapter
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsActivity
import kotlinx.android.synthetic.main.content_main.*

class MyTransactionsActivity : AppCompatActivity(), HomeContentAdapter.OnItemClickListener {

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var myTransactionsViewModel: MyTransactionsViewModel
    private lateinit var tabHolder: TabLayout
    private lateinit var adapter: HomeContentAdapter
    private lateinit var contentList: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_transactions)

        contentList = findViewById(R.id.content_list)
        contentList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = HomeContentAdapter()
        adapter.setOnItemClickListener(this)
        contentList.adapter = adapter
        contentList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        myTransactionsViewModel = ViewModelProviders.of(this, MyTransactionsViewModelFactory())
            .get(MyTransactionsViewModel::class.java)

        myTransactionsViewModel.transactionsFormState.observe(this, Observer {
            swipeRefresh.isRefreshing = false
            adapter.items = it.items
            empty_view.visibility = if(it.items.isEmpty()) View.VISIBLE else View.GONE
            adapter.notifyDataSetChanged()

            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.message)) {
                it.message?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        })

        tabHolder = findViewById(R.id.tab_holder)
        tabHolder.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                myTransactionsViewModel.filterObject.type = (tab?.position ?: 0) + 1
                myTransactionsViewModel.loadData()
            }

        })
        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            myTransactionsViewModel.loadData()
        }

    }

    override fun onResume() {
        super.onResume()
        myTransactionsViewModel.loadData()
    }

    override fun onClick(position: Int, item: ContentItem) {
        val intent = Intent(this, TransactionDetailsActivity::class.java)
        intent.putExtra("TRANSACTION", (item as TransactionItem).moneyDocument)
        startActivity(intent)
    }
}
