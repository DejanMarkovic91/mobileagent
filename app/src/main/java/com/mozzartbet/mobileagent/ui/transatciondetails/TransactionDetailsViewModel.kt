package com.mozzartbet.mobileagent.ui.transatciondetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainRepository
import com.mozzartbet.mobileagent.data.user
import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import java.util.*

class TransactionDetailsViewModel(
    private val mainRepository: MainRepository,
    private val loginRepository: LoginRepository
) : ViewModel() {

    lateinit var transaction: MoneyDocument
    private val _transactionForm = MutableLiveData<TransactionDetailsFormState>()
    val transactionDetailsFormState: LiveData<TransactionDetailsFormState> = _transactionForm

    private fun getTransactionDetails(id: Long) {
        mainRepository.getTransactionById(id)!!.subscribe({ resp ->
            run {
                transaction = resp
                _transactionForm.value =
                    TransactionDetailsFormState(
                        transaction = resp
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value =
                TransactionDetailsFormState(
                    error = throwable.localizedMessage
                )
        })
    }

    fun unassignTransaction(id: Long) {
        mainRepository.unassignTransaction(
            user!!.displayName, id
        )!!.subscribe({ resp ->
            run {
                getTransactionDetails(id)
                _transactionForm.value =
                    TransactionDetailsFormState(
                        message = if ("FAIL" == resp.status) resp.message else resp.status
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value =
                TransactionDetailsFormState(
                    error = throwable.localizedMessage
                )
        })
    }

    fun assignTransaction(id: Long) {
        mainRepository.assignTransaction(
            user!!.displayName, id
        )!!.subscribe({ resp ->
            run {
                getTransactionDetails(id)
                _transactionForm.value =
                    TransactionDetailsFormState(
                        message = if ("FAIL" == resp.status) resp.message else resp.status
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value =
                TransactionDetailsFormState(
                    error = throwable.localizedMessage
                )
        })
    }

    fun handleTransaction(id: Long, requestType: String, amount: Double, code: String) {
        mainRepository.handleTransaction(id, requestType, amount, code)!!.subscribe({ resp ->
            run {
                _transactionForm.value =
                    TransactionDetailsFormState(
                        handleMessage = if ("SUCCESS" == resp.status) {
                            resp.status
                        } else {
                            resp.message
                        }, userStatus = resp.userReposnse
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value = TransactionDetailsFormState(
                error = throwable.localizedMessage
            )
        })
    }

    fun loadHandledTransactionsForDate(date: Calendar) {
        mainRepository.getHandledTransactionsForDate(user!!.displayName, date).subscribe({ resp ->
            run {
                _transactionForm.value =
                    TransactionDetailsFormState(
                        handledItems = resp
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value = TransactionDetailsFormState(
                error = throwable.localizedMessage
            )
        })
    }

    fun payoutTransactionUserConfirmed() {
        mainRepository.payoutTransactionUserConfirmed()!!.subscribe({ resp ->
            run {
                _transactionForm.value =
                    TransactionDetailsFormState(
                        handleMessage = if ("SUCCESS" == resp.status) {
                            resp.status
                        } else {
                            resp.message
                        }
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionForm.value = TransactionDetailsFormState(
                error = throwable.localizedMessage
            )
        })
    }

}