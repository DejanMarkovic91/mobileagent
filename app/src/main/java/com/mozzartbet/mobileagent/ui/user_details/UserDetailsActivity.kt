package com.mozzartbet.mobileagent.ui.user_details

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.ui.adapter.HomeContentAdapter
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsActivity
import kotlinx.android.synthetic.main.activity_transaction_details.*

class UserDetailsActivity : AppCompatActivity(), HomeContentAdapter.OnItemClickListener {

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var inputField: EditText
    private lateinit var userDetailsViewModel: UserDetailsViewModel
    private lateinit var adapter: HomeContentAdapter
    private lateinit var contentList: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        contentList = findViewById(R.id.content)
        contentList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = HomeContentAdapter()
        adapter.setOnItemClickListener(this)
        contentList.adapter = adapter
        contentList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        inputField = findViewById(R.id.input_text)

        userDetailsViewModel = ViewModelProviders.of(this, UserDetailsViewModelFactory())
            .get(UserDetailsViewModel::class.java)

        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            userDetailsViewModel.loadData(inputField.text.toString())
        }

        findViewById<AppCompatButton>(R.id.submit).setOnClickListener {
            swipeRefresh.isRefreshing = true
            userDetailsViewModel.loadData(inputField.text.toString())
        }

        userDetailsViewModel.userDetailsFormState.observe(this@UserDetailsActivity, Observer {
            swipeRefresh.isRefreshing = false
            adapter.items = it.items!!
            adapter.notifyDataSetChanged()

            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.message)) {
                it.message?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
        )
    }

    override fun onClick(position: Int, item: ContentItem) {
        val intent = Intent(this, TransactionDetailsActivity::class.java)
        intent.putExtra("TRANSACTION", (item as TransactionItem).moneyDocument)
        startActivity(intent)
    }
}
