package com.mozzartbet.mobileagent.ui.transatciondetails

import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import com.mozzartbet.mobileagent.network.dto.UserStatusResponse
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem

data class TransactionDetailsFormState(
    val items: MoneyDocument? = null,
    val error: String? = null,
    val message: String? = null,
    val handleMessage: String? = null,
    val transaction: MoneyDocument? = null,
    val handledItems: List<ContentItem>? = null,
    val userStatus: UserStatusResponse? = null
)