package com.mozzartbet.mobileagent.ui.vip

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainRepository
import com.mozzartbet.mobileagent.data.user
import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsFormState

class VIPViewModel(
    private val mainRepository: MainRepository,
    private val loginRepository: LoginRepository
) : ViewModel() {
    lateinit var transaction: MoneyDocument
    private val _vipForm = MutableLiveData<VIPFormState>()
    val vipFormState: LiveData<VIPFormState> = _vipForm

    fun createTransaction(username: String, amount: Double) {
        mainRepository.createTransaction(username, amount, user!!.displayName)!!.subscribe({ resp ->
            run {
                if ("SUCCESS" != resp.status) {
                    _vipForm.value =
                        VIPFormState(
                            message = if (!TextUtils.isEmpty(resp.message)) resp.message else resp.status
                        )
                } else {
                    _vipForm.value =
                        VIPFormState(
                            transaction = resp.paymentTransactionInfo!!
                        )
                }
            }
        }, { throwable ->
            throwable.printStackTrace()
            run {
                _vipForm.value =
                    VIPFormState(
                        error = throwable.localizedMessage
                    )
            }
        })
    }

    fun handleTransaction(transaction: MoneyDocument) {
        mainRepository.handleTransaction(
            transaction.id,
            transaction.requestType,
            transaction.plannedAmount,
            null
        )!!.subscribe({ resp ->
            run {
                _vipForm.value =
                    VIPFormState(
                        handleMessage = if ("SUCCESS" == resp.status) resp.status else resp.message
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            run {
                _vipForm.value =
                    VIPFormState(
                        error = throwable.localizedMessage
                    )
            }
        })
    }

    fun unassignTransaction(id: Long) {
        mainRepository.unassignTransaction(
            user!!.displayName, id
        )!!.subscribe({ resp ->
            run {
                _vipForm.value =
                    VIPFormState(
                        handleMessage = if ("SUCCESS" == resp.status) resp.status else resp.message
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _vipForm.value =
                VIPFormState(
                    error = throwable.localizedMessage
                )
        })
    }

}