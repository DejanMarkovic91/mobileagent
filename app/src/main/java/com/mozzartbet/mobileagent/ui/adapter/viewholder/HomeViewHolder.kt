package com.mozzartbet.mobileagent.ui.adapter.viewholder

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mozzartbet.mobileagent.R

class HomeViewHolder: RecyclerView.ViewHolder {

    public var email: TextView?
    public var username: TextView?
    public var money: TextView?
    public var shareId: TextView?
    public var way: TextView?

    constructor(itemView: View) : super(itemView) {
        email = itemView.findViewById(R.id.email)
        username = itemView.findViewById(R.id.username)
        money = itemView.findViewById(R.id.money)
        shareId = itemView.findViewById(R.id.share_id)
        way = itemView.findViewById(R.id.way)
    }
}