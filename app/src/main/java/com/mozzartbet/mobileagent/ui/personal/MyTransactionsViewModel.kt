package com.mozzartbet.mobileagent.ui.personal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainRepository
import com.mozzartbet.mobileagent.data.user
import com.mozzartbet.mobileagent.ui.home.FilterObject

class MyTransactionsViewModel(
    private val mainRepository: MainRepository,
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _transactionsForm = MutableLiveData<MyTransactionsFormState>()
    val transactionsFormState: LiveData<MyTransactionsFormState> = _transactionsForm
    val filterObject = FilterObject()

    fun loadData() {
        mainRepository.getAssignedDocuments(
            user!!.displayName,
            if (filterObject.type == 1) "Payin" else "Payout"
        )!!.subscribe({ items ->
            run {
                _transactionsForm.value =
                    MyTransactionsFormState(
                        items = items
                    )
            }
        }, { throwable ->
            throwable.printStackTrace()
            _transactionsForm.value = MyTransactionsFormState(
                error = throwable.localizedMessage
            )
        })
    }

}