package com.mozzartbet.mobileagent.ui.transatciondetails

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import kotlinx.android.synthetic.main.activity_transaction_details.*


class TransactionDetailsActivity : AppCompatActivity() {

    private lateinit var transactionDetailsViewModel: TransactionDetailsViewModel
    private lateinit var textDetails: TextView
    private lateinit var bottomBar: TabLayout
    private var first = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_details)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        textDetails = findViewById(R.id.text_content)
        bottomBar = findViewById(R.id.bottom_navigation)

        transactionDetailsViewModel =
            ViewModelProviders.of(this, TransactionDetailsViewModelFactory())
                .get(TransactionDetailsViewModel::class.java)

        transactionDetailsViewModel.transactionDetailsFormState.observe(this, Observer {

            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        textDetails,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (it.handleMessage != null) {
                when (it.handleMessage) {
                    "UserBlockedCheck" -> {
                        val taskEditText = EditText(this);
                        val dialog = AlertDialog.Builder(this)
                            .setTitle(R.string.identity_check)
                            .setMessage(R.string.input_identity_number)
                            .setView(taskEditText)
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->

                            }
                            .create();
                        dialog.show()
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { _ ->
                            run {
                                if (taskEditText.text.toString() == it.userStatus!!.identityNumber) {
                                    AlertDialog.Builder(this)
                                        .setMessage(
                                            getString(
                                                R.string.are_you_sure_verification,
                                                it.userStatus.firstName + " " + it.userStatus.lastName,
                                                it.userStatus.identityNumber,
                                                it.userStatus.username,
                                                it.userStatus.amount,
                                                getString(R.string.currency)
                                            )
                                        )
                                        .setPositiveButton(R.string.yes) { _: DialogInterface, _: Int ->
                                            run {
                                                transactionDetailsViewModel.payoutTransactionUserConfirmed()
                                                dialog.cancel()
                                            }
                                        }
                                        .setNegativeButton(R.string.no) { _: DialogInterface, _: Int ->
                                            dialog.cancel()
                                        }
                                        .show()
                                } else {
                                    Toast.makeText(
                                        this,
                                        R.string.wrong_identity_number,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                    }
                    "SUCCESS" -> {
                        Toast.makeText(this, R.string.success_transaction, Toast.LENGTH_SHORT)
                            .show()
                        finish()
                        return@Observer
                    }
                    else -> {
                        it.handleMessage?.let { it1 ->
                            Snackbar.make(
                                textDetails,
                                it1,
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }

            it.message?.let { it1 ->
                if (it1 != "SUCCESS")
                    Snackbar.make(
                        textDetails,
                        it1,
                        Snackbar.LENGTH_SHORT
                    ).show()

            }

            if (it.transaction != null) {
                first = true
                refreshTransactionViews()
            }
        })

        bottomBar.addOnTabSelectedListener(
            object : TabLayout.OnTabSelectedListener {
                override fun onTabReselected(tab: TabLayout.Tab?) {
                    handleTabClick(tab)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    if (first) {
                        first = false
                        return
                    }
                    handleTabClick(tab)
                }
            })
    }

    private fun handleTabClick(tab: TabLayout.Tab?) {
        if (tab!!.position == 2) {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse(
                "tel:" + (transactionDetailsViewModel.transaction.mobileNumber?.replace(
                    "/",
                    ""
                ) ?: "")
            )
            startActivity(intent)
        } else if (tab!!.position == 1) {
            val view = LayoutInflater.from(this@TransactionDetailsActivity)
                .inflate(R.layout.accept_dialog, null) as View
            AlertDialog.Builder(this@TransactionDetailsActivity, R.style.DialogTheme)
                .setTitle(R.string.handle_tranaction)
                .setView(view)
                .setPositiveButton(
                    R.string.yes
                ) { _, _ ->
                    run {
                        transactionDetailsViewModel.handleTransaction(
                            transactionDetailsViewModel.transaction.id,
                            transactionDetailsViewModel.transaction.requestType,
                            (view.findViewById(R.id.amount) as EditText).text.toString().toDouble(),
                            (view.findViewById(R.id.code) as EditText).text.toString()
                        )
                    }
                }.setNegativeButton(
                    R.string.no
                ) { _, _ ->

                }
                .show()

            val text: TextView = view.findViewById(R.id.message)
            text.text = getString(R.string.did_you_handle_transaction)
            val amount: EditText = view.findViewById(R.id.amount)
            amount.setText(
                transactionDetailsViewModel.transaction.plannedAmount.toString()
            )
            (view.findViewById(R.id.amount_holder) as View).visibility =
                if (transactionDetailsViewModel.transaction.requestType == "Payin")
                    View.VISIBLE else View.GONE
            (view.findViewById(R.id.code_holder) as View).visibility =
                if (transactionDetailsViewModel.transaction.requestType == "Payout")
                    View.VISIBLE else View.GONE
        } else {
            if (transactionDetailsViewModel.transaction.requestStatus == "Assigned") {
                transactionDetailsViewModel.unassignTransaction(transactionDetailsViewModel.transaction.id)
            } else {
                transactionDetailsViewModel.assignTransaction(transactionDetailsViewModel.transaction.id)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        first = true
        try {
            transactionDetailsViewModel.transaction = intent.getParcelableExtra("TRANSACTION")
        } catch (e: Exception) {
            try {
                transactionDetailsViewModel.transaction = Gson().fromJson(
                    intent.extras?.getString("TRANSACTION"),
                    MoneyDocument::class.java
                )
            } catch (e1: Exception) {
                e.printStackTrace()
                e1.printStackTrace()
            }
        }

        refreshTransactionViews()
    }

    private fun refreshTransactionViews() {
        textDetails.text = transactionDetailsViewModel.transaction.toString(this)
        populateBottomBar()
    }

    private fun populateBottomBar() {
        bottomBar.removeAllTabs()
        if (transactionDetailsViewModel.transaction.requestStatus == "Assigned") {
            bottomBar.addTab(bottomBar.newTab().setText(R.string.unassign_transaction))
            bottomBar.addTab(bottomBar.newTab().setText(R.string.handle_tranaction))
            bottomBar.addTab(bottomBar.newTab().setText(R.string.call_user))
        } else if (transactionDetailsViewModel.transaction.requestStatus != "Completed") {
            bottomBar.addTab(bottomBar.newTab().setText(R.string.assign_transaction))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        if (id == R.id.action_share) {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(
                Intent.EXTRA_TEXT,
                transactionDetailsViewModel.transaction.id
            )
            startActivity(
                Intent.createChooser(
                    shareIntent,
                    getString(R.string.send_to)
                )
            )

            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
