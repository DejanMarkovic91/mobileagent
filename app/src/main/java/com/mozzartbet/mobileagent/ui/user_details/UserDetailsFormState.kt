package com.mozzartbet.mobileagent.ui.user_details

import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem

data class UserDetailsFormState(
    val items: List<ContentItem>? = ArrayList(),
    val error: String? = null,
    val message: String? = null
)