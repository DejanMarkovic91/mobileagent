package com.mozzartbet.mobileagent.ui.adapter.item

object ContentConstants {
    const val USER_ITEM = 1
    const val TRANSACTION_ITEM = 2
    const val HANDLED_TRANSACTION_ITEM = 3
}

open class ContentItem(val type: Int) 