package com.mozzartbet.mobileagent.ui.home

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.firebase.messaging.FirebaseMessaging
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.data.user
import com.mozzartbet.mobileagent.print.PrintHelper
import com.mozzartbet.mobileagent.ui.adapter.HomeContentAdapter
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import com.mozzartbet.mobileagent.ui.handled_transactions.HandledTransactionsActivity
import com.mozzartbet.mobileagent.ui.login.LoginActivity
import com.mozzartbet.mobileagent.ui.personal.MyTransactionsActivity
import com.mozzartbet.mobileagent.ui.slot.ConnectSlotActivity
import com.mozzartbet.mobileagent.ui.transatciondetails.TransactionDetailsActivity
import com.mozzartbet.mobileagent.ui.user_details.UserDetailsActivity
import com.mozzartbet.mobileagent.ui.vip.VIPActivity
import kotlinx.android.synthetic.main.content_main.*
import ru.dimorinny.floatingtextbutton.FloatingTextButton


class MainActivity : AppCompatActivity(), HomeContentAdapter.OnItemClickListener {

    private lateinit var fab: FloatingTextButton
    private lateinit var mainViewModel: MainViewModel
    private lateinit var searchView: SearchView
    private lateinit var contentList: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var tabHolder: TabLayout
    private lateinit var adapter: HomeContentAdapter
    private var printHelper = PrintHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        fab = findViewById(R.id.my_transactions)
        fab.setOnClickListener { startActivity(Intent(this, MyTransactionsActivity::class.java)) }
        contentList = findViewById(R.id.content_list)
        contentList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = HomeContentAdapter()
        adapter.setOnItemClickListener(this)
        contentList.adapter = adapter
        contentList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        mainViewModel = ViewModelProviders.of(this, MainViewModelFactory())
            .get(MainViewModel::class.java)

        mainViewModel.mainFormState.observe(this@MainActivity, Observer {
            swipeRefresh.isRefreshing = false
            adapter.items = it.items
            empty_view.visibility = if (it.items.isEmpty()) View.VISIBLE else View.GONE
            adapter.notifyDataSetChanged()

            if (!TextUtils.isEmpty(it.error)) {
                it.error?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1, Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.message)) {
                it.message?.let { it1 ->
                    Snackbar.make(
                        contentList,
                        it1,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            if (!TextUtils.isEmpty(it.updateURL)) {
                AlertDialog.Builder(this)
                    .setMessage(R.string.download_update)
                    .setPositiveButton(
                        R.string.download
                    ) { _: DialogInterface, _: Int ->
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(it.updateURL)
                        )
                        startActivity(browserIntent)
                    }
                    .setCancelable(true)
                    .show()
            }
        })
        tabHolder = findViewById(R.id.tab_holder)
        tabHolder.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                mainViewModel.filterObject.type = (tab?.position ?: 0) + 1
                mainViewModel.loadData()
            }

        })
        swipeRefresh = findViewById(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            mainViewModel.loadData()
        }

        mainViewModel.checkForUpdate()
    }

    override fun onResume() {
        super.onResume()
        printHelper.initSunmiPrinterService(this)
        mainViewModel.loadData()
    }

    override fun onPause(){
        super.onPause()
        printHelper.deInitSunmiPrinterService(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        searchView.setOnCloseListener {
            searchView.onActionViewCollapsed()
            true
        }

        val searchPlate =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = getString(R.string.search)
        val searchPlateView: View =
            searchView.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query != null) {
                    mainViewModel.filterObject.searchQuery = query.toLowerCase()
                } else {
                    mainViewModel.filterObject.searchQuery = ""
                }
                mainViewModel.loadData()
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_sort) {
            AlertDialog.Builder(this)
                .setTitle(R.string.sort_by)
                .setItems(
                    resources.getStringArray(R.array.sort_values)
                ) { _, position ->
                    run {
                        mainViewModel.sortType = position
                        mainViewModel.loadData()
                    }
                }
                .show()
            return true;
        }
        if (item.itemId == R.id.action_settings) {
            val to = "dejan.markovic@mozzartbet.com"
            val subject = "Kontakt Aplikacija Mobilni Agenti"
            val body = ""
            val mailTo = "mailto:" + to +
                    "?&subject=" + Uri.encode(subject) +
                    "&body=" + Uri.encode(body)
            val emailIntent = Intent(Intent.ACTION_VIEW)
            emailIntent.data = Uri.parse(mailTo)
            startActivity(emailIntent)
            return true
        }
        if (item.itemId == R.id.action_vip) {
            startActivity(Intent(this, VIPActivity::class.java))
            return true
        }
        if (item.itemId == R.id.action_handled_transactions) {
            startActivity(Intent(this, HandledTransactionsActivity::class.java))
            return true
        }
        if (item.itemId == R.id.action_user_details) {
            startActivity(Intent(this, UserDetailsActivity::class.java))
            return true
        }
        if (item.itemId == R.id.action_logout) {
            FirebaseMessaging.getInstance().subscribeToTopic(user!!.displayName)
                .addOnCompleteListener {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
            return true
        }
        if (item.itemId == R.id.action_print_test) {
            if(printHelper.checkPrinterStatus(this)) {
                //printHelper.printQr("https://www.mozzartbet.com/sr#/ticket-status/1805-01137-4403-020",11,0)
                printHelper.printText(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                            "\n", 24.0F, false, false
                )
                printHelper.print3Line()
            }
            return true
        }
        if (item.itemId == R.id.action_connect_slot) {
            startActivity(Intent(this, ConnectSlotActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.onActionViewCollapsed()
        } else {
            super.onBackPressed()
        }
    }

    override fun onClick(position: Int, item: ContentItem) {
        val intent = Intent(this, TransactionDetailsActivity::class.java)
        intent.putExtra("TRANSACTION", (item as TransactionItem).moneyDocument)
        startActivity(intent)
    }
}
