package com.mozzartbet.mobileagent.ui.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.mozzartbet.mobileagent.R
import com.mozzartbet.mobileagent.ui.adapter.item.ContentConstants
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.HandledTransactionItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import com.mozzartbet.mobileagent.ui.adapter.viewholder.HomeViewHolder

class HomeContentAdapter : RecyclerView.Adapter<HomeViewHolder>() {

    var items: List<ContentItem> = ArrayList();
    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        return HomeViewHolder(
            LayoutInflater.from(parent.context).inflate(getResource(viewType), parent, false)
        )
    }

    private fun getResource(viewType: Int): Int {
        if (viewType == ContentConstants.USER_ITEM) return R.layout.item_user
        else if (viewType == ContentConstants.TRANSACTION_ITEM) return R.layout.item_transaction_item
        else if (viewType == ContentConstants.HANDLED_TRANSACTION_ITEM) return R.layout.item_handled_transaction
        else return 0
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        var item = items[position]
        if (item.type == ContentConstants.USER_ITEM) {

        } else if (item.type == ContentConstants.TRANSACTION_ITEM) {
            val transactionItem = item as TransactionItem
            holder.username?.text =
                "ID: ${transactionItem.moneyDocument.id}"
            holder.money?.text =
                "${transactionItem.moneyDocument.address}"
            holder.shareId?.setOnClickListener {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    (items[position] as TransactionItem).moneyDocument.id
                )
                holder.itemView.context.startActivity(
                    Intent.createChooser(
                        shareIntent,
                        holder.itemView.context.getString(R.string.send_to)
                    )
                )

            }
            holder.username?.setOnClickListener {
                listener?.onClick(position, item)
            }
            holder.money?.setOnClickListener {
                listener?.onClick(position, item)
            }
        } else {
            //handled
            val transactionItem = item as HandledTransactionItem
            holder.username?.text =
                "${holder.itemView.context.getString(R.string.username)}: ${transactionItem.moneyDocument.player?.username}"
            holder.money?.text =
                "${transactionItem.moneyDocument.payedAmount}"
            holder.way?.text =
                "${if(transactionItem.moneyDocument.requestType == "Payin") "U" else "I"}"
            holder.username?.setOnClickListener {
                listener?.onClick(position, item)
            }
            holder.money?.setOnClickListener {
                listener?.onClick(position, item)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    interface OnItemClickListener {

        fun onClick(position: Int, item: ContentItem)

    }
}