package com.mozzartbet.mobileagent.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mozzartbet.mobileagent.BuildConfig
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainRepository
import com.mozzartbet.mobileagent.data.user

class MainViewModel(
    private val mainRepository: MainRepository,
    private val loginRepository: LoginRepository
) : ViewModel() {

    var sortType: Int = -1
    private val _mainForm = MutableLiveData<MainFormState>()
    val mainFormState: LiveData<MainFormState> = _mainForm
    val filterObject = FilterObject()

    fun loadData() {
        if (user != null) {
            mainRepository.getUnassignedDocuments(
                user!!.displayName,
                if (filterObject.type == 1) "Payin" else "Payout",
                filterObject.searchQuery,
                sortType
            )!!.subscribe({ items ->
                run {
                    _mainForm.value =
                        MainFormState(
                            items = items
                        )
                }
            }, { throwable ->
                throwable.printStackTrace()
                _mainForm.value = MainFormState(
                    error = throwable.localizedMessage
                )
            })
        }
    }

    fun checkForUpdate() {
        mainRepository.getApplicationConfig()!!.subscribe({ config ->
            run {
                try {
                    val con = config.updateConfigs?.get(0)
                    if (con != null) {
                        if (con.version?.toFloat()!! > BuildConfig.VERSION_NAME.toFloat()) {
                            _mainForm.value =
                                MainFormState(
                                    updateURL = con.fileUrl
                                )
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }, { throwable ->
            _mainForm.value = MainFormState(
                error = throwable.localizedMessage
            )
        })
    }

}