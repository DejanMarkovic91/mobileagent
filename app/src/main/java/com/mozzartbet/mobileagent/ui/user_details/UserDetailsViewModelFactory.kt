package com.mozzartbet.mobileagent.ui.user_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mozzartbet.mobileagent.data.LoginDataSource
import com.mozzartbet.mobileagent.data.LoginRepository
import com.mozzartbet.mobileagent.data.MainDataSource
import com.mozzartbet.mobileagent.data.MainRepository

class UserDetailsViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserDetailsViewModel::class.java)) {
            return UserDetailsViewModel(
                mainRepository = MainRepository(
                    dataSource = MainDataSource()
                ),
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource()
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}