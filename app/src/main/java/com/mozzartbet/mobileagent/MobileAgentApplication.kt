package com.mozzartbet.mobileagent

import android.app.Application
import android.os.Build
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import java.util.*


class MobileAgentApplication : MultiDexApplication() {
    lateinit var locale: Locale

    override fun onCreate() {
        super.onCreate()

        setUpLocale()

        //stetho init
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this)

        //fonts init
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/ProximaNova-Regular_WS.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

    }

    private fun setUpLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            locale =
                Locale.Builder().setLanguage(getString(R.string.language))
                    .setRegion(getString(R.string.region))
                    .setScript(getString(R.string.script))
                    .build()
        } else {
            for (checkLocale in Locale.getAvailableLocales()) {
                if (checkLocale.isO3Language == getString(R.string.iso3_language) && checkLocale.country == getString(
                        R.string.script
                    ) && checkLocale.variant == ""
                ) {
                    locale = checkLocale
                }
            }
        }
        Locale.setDefault(locale)
    }
}