package com.mozzartbet.mobileagent.helper;

import android.content.Context;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.StringTokenizer;

import javax.inject.Inject;

public class CredentialStorage {
    private static final String CREDENTIALS_FILE_NAME = "sklj.dat";
    private static final String CRYPT_KEY = "wolfgang";
    private static final int VERSION = 1;

    private File credentialsFile;

    @Inject
    public CredentialStorage(Context context) {
        this.credentialsFile = new File(context.getFilesDir(), CREDENTIALS_FILE_NAME);
    }

    public boolean storeCredentials(String username, String password) {
        OutputStream outS = null;
        boolean isEncrypted = false;
        try {
            outS = new FileOutputStream(credentialsFile);
            AESCrypt aes = new AESCrypt(CRYPT_KEY);
            InputStream inS = new ByteArrayInputStream((username + "#" + password).getBytes());
            aes.encrypt(VERSION, inS, outS);
            isEncrypted = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isEncrypted;
    }

    public String[] readCredentials() {
        try {
            OutputStream out = new ByteArrayOutputStream();
            AESCrypt aes = new AESCrypt(CRYPT_KEY);
            aes.decrypt(credentialsFile.length(), new FileInputStream(credentialsFile), out);
            String usrAndPass = out.toString();
            StringTokenizer tokenizer = new StringTokenizer(usrAndPass, "#");
            String username = tokenizer.nextToken();
            String password = tokenizer.nextToken();
            return new String[]{username, password};
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
