package com.mozzartbet.mobileagent.network.dto

data class VIPResponse(
    val status: String?,
    val message: String?,
    val paymentTransactionInfo: MoneyDocument?
) {
    constructor(message: String) : this(null, message, null)
}