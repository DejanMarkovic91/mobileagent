package com.mozzartbet.mobileagent.network.dto

data class HandleRequest(
    val requestId: Long? = null,
    val amount: Double? = null,
    val paymentCode: String? = null
)