package com.mozzartbet.mobileagent.network.dto

data class ApplicationSettings(val test: String? = null)
