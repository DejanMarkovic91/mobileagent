package com.mozzartbet.mobileagent.network.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(
    val id: Long,
    val firstname:String,
    val lastname: String,
    val username:String,
    val identityNumber:String,
    val dateOfBirth:Long
): Parcelable