package com.mozzartbet.mobileagent.network.dto

data class UpdateConfig(
    val module: String? = null,
    val version: String? = null,
    val fileSize: Long = 0,
    val fileUrl: String? = null,
    val isMandatory: Boolean = false,
    val checkSum: String? = null
)