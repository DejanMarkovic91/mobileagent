package com.mozzartbet.mobileagent.network.dto

class UserStatusRequest (
    val paymentCode: String? = null,
    val requestId: Long? = 0
)