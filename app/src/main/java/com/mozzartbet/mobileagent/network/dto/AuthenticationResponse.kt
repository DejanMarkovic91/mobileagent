package com.mozzartbet.mobileagent.network.dto

data class AuthenticationResponse(
    val status: String? = null,
    val userId: Int? = 0,
    val sessionId: String? = null,
    val omegaSessionKey: String? = null,
    val displayName: String? = null,
    val clubActivated: Boolean? = true,
    val countryId: Int? = 0,
    val jwt: String? = null
)