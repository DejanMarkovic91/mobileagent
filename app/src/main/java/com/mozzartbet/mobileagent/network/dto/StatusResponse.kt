package com.mozzartbet.mobileagent.network.dto

data class StatusResponse(
    val status: String? = null,
    val message: String?,
    val userReposnse: UserStatusResponse? = null
)