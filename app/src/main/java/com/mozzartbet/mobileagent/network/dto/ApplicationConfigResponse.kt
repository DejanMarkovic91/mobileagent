package com.mozzartbet.mobileagent.network.dto

data class ApplicationConfigResponse(
    var updateConfigs: List<UpdateConfig>? = null,
    var applicationSettings: ApplicationSettings? = null
)