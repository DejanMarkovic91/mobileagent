package com.mozzartbet.mobileagent.network.dto

data class CreateRequest(
    val username: String? = null,
    val amount: Double? = null,
    val agentUsername: String? = null
)