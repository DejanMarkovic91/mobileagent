package com.mozzartbet.mobileagent.network

import com.mozzartbet.mobileagent.network.dto.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("login")
    fun createSession(@Body request: AuthenticationRequest?): Call<Void?>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @GET
    fun getModuleUpdateConfig(
        @Url url: String?,
        @Query("marketId") marketId: Int?,
        @Query("lcMemberId") lcMemberId: Long?
    ): Call<ApplicationConfigResponse?>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @GET("mobile-agents/find-requests")
    fun getRequests(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Query("agentUsername") agentUsername: String?,
        @Query("requestType") requestType: String?,//Payin, Payout
        @Query("requestStatus") requestStatus: String?
//        Created, - kreirani, spremni za preuzimanje
//        Assigned, - preuzeti od strane agenta
//        Completed - obradjeni, zavrsena uplata/isplata
//    Cancelled - otkazani (iz Amadeusa se desilo otkazivanje, neko otkazao rucno, istekla rezervacija..)

    ): Call<List<MoneyDocument>>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/assign-request")
    fun assignRequest(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: AssignRequest
    ): Call<StatusResponse>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/unassign-request")
    fun unassignRequest(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: AssignRequest
    ): Call<StatusResponse>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/checkIfPlayerIsBlocked")
    fun checkUserBlocked(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: UserStatusRequest
    ): Call<UserStatusResponse>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/completeAdvancePayin ")
    fun processRequestPayin(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: HandleRequest
    ): Call<StatusResponse>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/completeAdvancePayout ")
    fun processRequestPayout(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: HandleRequest
    ): Call<StatusResponse>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @GET("mobile-agents/getRequest/{id}")
    fun getTransactionById(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Path("id") id: Long
    ): Call<MoneyDocument>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @GET("mobile-agents/getLastTransactionsForPlayer")
    fun getLastTransactionsForPlayer(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Query("username") username: String?,
        @Query("identityNumber") identityNumber: String?
    ): Call<List<MoneyDocument>>?

    @Headers(
        "Content-Type: application/json",
        "Content-type: application/json; charset=UTF-8"
    )
    @POST("mobile-agents/createPayinUnannounced")
    fun createTransactionUnannounced(
        @Header("X-AUTH-TOKEN") jwt: String,
        @Body request: CreateRequest
    ): Call<VIPResponse>?
}