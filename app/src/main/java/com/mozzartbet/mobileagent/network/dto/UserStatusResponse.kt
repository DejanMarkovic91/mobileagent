package com.mozzartbet.mobileagent.network.dto

class UserStatusResponse (
    val lcMemberId: Long? = 0,
    val identityNumber: String? = null,
    val username: String? = null,
    val state: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val amount: Double? = 0.0,
    val isBlocked: Boolean? = false
)