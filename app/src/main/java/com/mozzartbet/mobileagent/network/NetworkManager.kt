package com.mozzartbet.mobileagent.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.mozzartbet.mobileagent.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkManager {

    var service: ApiService
        private set

    constructor(){
        val client = OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .addNetworkInterceptor(StethoInterceptor())
            .build()


        service = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL) //produkcija
            //.baseUrl(BuildConfig.SDEPLOY_URL)//sdeploy
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiService::class.java)
    }

}