package com.mozzartbet.mobileagent.network.dto

data class AssignRequest(val agentUsername: String? = null, val requestId: Long? = null)