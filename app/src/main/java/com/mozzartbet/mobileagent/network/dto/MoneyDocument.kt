package com.mozzartbet.mobileagent.network.dto

import android.content.Context
import android.os.Parcelable
import com.mozzartbet.mobileagent.R
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

val dateFormat = SimpleDateFormat("HH:mm dd.MM.yyyy.")
val dateUserFormat = SimpleDateFormat("dd.MM.yyyy.")

@Parcelize
data class MoneyDocument(

    val id: Long,
    val dateCreated: Long,
    val processingDate: Long,
    val expirationDate: Long,
    val player: Player?,
    val plannedAmount: Double,
    val payedAmount: Double,
    val requestType: String,
    val requestStatus: String,
    val address: String?,
    val mobileNumber: String?,
    val description: String?,
    val payableFrom: Long
) : Parcelable {
    fun toString(context: Context): String {
        var result = "${context.getString(R.string.id)}: $id" +
                "\n${context.getString(R.string.identity_number)}: ${player!!.identityNumber}" +
                "\n${context.getString(R.string.username)}: " + "${player.username}" +
                "\n${context.getString(R.string.address)}: " + address +
                "\n${context.getString(R.string.mobile_number)}: " + mobileNumber +
                "\n${context.getString(R.string.date_created)}: " +
                "${dateFormat.format(Date(dateCreated))}" +
                "\n${context.getString(R.string.transaction_type)}: ${if (requestType == "Payin")
                    context.getString(R.string.payin_type).toUpperCase() else
                    context.getString(R.string.payout_type).toUpperCase()} " +
                "\n${context.getString(R.string.amount)}: " + "$plannedAmount" +
                "\n${context.getString(R.string.desc)}: " + "$description" +
                "\n${context.getString(R.string.valid_from)}: " + "${dateFormat.format(
            Date(
                payableFrom
            )
        )}" +
                "\n${context.getString(R.string.valid_to)}: " + "${dateFormat.format(
            Date(
                expirationDate
            )
        )}" +
                "\n${context.getString(R.string.status)}: " + requestStatus
        if (processingDate > 0) {
            result +=
                "\n${context.getString(R.string.handled)}: " + "${dateFormat.format(
                    Date(
                        processingDate
                    )
                )}"
        }
        return result
    }

    fun showUserDetails(context: Context): String {
        return "${context.getString(R.string.identity_number)}: ${player!!.identityNumber}" +
                "\n${context.getString(R.string.name_and_last)}: " + player.firstname + " " + player.lastname +
                "\n${context.getString(R.string.date_of_birth)}: " + dateUserFormat.format(
            Date(
                player.dateOfBirth
            )
        ) +
                "\n${context.getString(R.string.amount)}: " + plannedAmount
//                "\n${context.getString(R.string.mobile_number)}: " + mobileNumber
    }
}