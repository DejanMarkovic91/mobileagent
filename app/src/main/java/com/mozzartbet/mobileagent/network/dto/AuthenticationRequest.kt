package com.mozzartbet.mobileagent.network.dto

data class AuthenticationRequest (
    val username: String? = null,
    val password: String? = null
)