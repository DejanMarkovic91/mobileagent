package com.mozzartbet.mobileagent.data

import android.text.TextUtils
import androidx.core.text.isDigitsOnly
import com.mozzartbet.mobileagent.network.dto.ApplicationConfigResponse
import com.mozzartbet.mobileagent.network.dto.MoneyDocument
import com.mozzartbet.mobileagent.network.dto.StatusResponse
import com.mozzartbet.mobileagent.network.dto.VIPResponse
import com.mozzartbet.mobileagent.ui.adapter.item.ContentItem
import com.mozzartbet.mobileagent.ui.adapter.item.HandledTransactionItem
import com.mozzartbet.mobileagent.ui.adapter.item.TransactionItem
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class MainRepository(val dataSource: MainDataSource) {

    fun getApplicationConfig(): @NonNull Observable<ApplicationConfigResponse>? {
        return dataSource.getApplicationConfig()
    }

    fun getUnassignedDocuments(
        username: String,
        requestType: String,
        query: String,
        sort: Int
    ): @NonNull Observable<List<ContentItem>>? {
        return dataSource.getUnassignedRequests(username, requestType)!!
            .map { t ->
                val items = ArrayList<ContentItem>()

                val now = Calendar.getInstance().timeInMillis
                val iterator = t.iterator()
                val newList = ArrayList<MoneyDocument>()

                for (i in iterator) {
                    if (i.expirationDate > now) {
                        newList.add(i)
                    }
                }

                try {
                    if (sort >= 0) {
                        when (sort) {
                            0 -> {
                                newList.sortWith(Comparator { p0, p1 -> p1.dateCreated.compareTo(p0.dateCreated) })
                            }
                            1 -> {
                                newList.sortWith(Comparator { p0, p1 -> p0.dateCreated.compareTo(p1.dateCreated) })
                            }
                            2 -> {
                                newList.sortWith(Comparator { p0, p1 -> p0.player!!.username.compareTo(p1.player!!.username) })
                            }
                            3 -> {
                                newList.sortWith(Comparator { p0, p1 -> p1.player!!.username.compareTo(p0.player!!.username) })
                            }
                            4 -> {
                                newList.sortWith(Comparator { p0, p1 -> p0.plannedAmount.compareTo(p1.plannedAmount) })
                            }
                            5 -> {
                                newList.sortWith(Comparator { p0, p1 -> p1.plannedAmount.compareTo(p0.plannedAmount) })
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                for (item in newList!!) {
                    val transaction = TransactionItem(item)
                    if (TextUtils.isEmpty(query) || transaction.contains(query))
                        items.add(transaction)
                }
                items
            }
    }

    fun getAssignedDocuments(
        username: String,
        requestType: String
    ): @NonNull Observable<List<ContentItem>>? {
        return dataSource.getAssignedRequestsToUser(username, requestType)!!
            .map { t ->
                val items = ArrayList<ContentItem>()
                for (item in t!!) {
                    items.add(TransactionItem(item))
                }
                items
            }
    }

    fun handleTransaction(
        id: Long,
        requestType: String,
        amount: Double,
        code: String?
    ): @NonNull Observable<StatusResponse>? {
        return dataSource.handleTransaction(requestType, id, amount, code)
    }

    fun unassignTransaction(username: String, id: Long): @NonNull Observable<StatusResponse>? {
        return dataSource.unassignTransaction(username, id)
    }

    fun assignTransaction(username: String, id: Long): @NonNull Observable<StatusResponse>? {
        return dataSource.assignTransaction(username, id)
    }

    fun getTransactionById(id: Long): @NonNull Observable<MoneyDocument> {
        return dataSource.getTransactionById(id)
    }

    fun getUserDetails(username: String, text: String): @NonNull Observable<List<ContentItem>>? {
        return dataSource.getUserDetails(
            username,
            if (text.isDigitsOnly()) text else null,
            if (text.isDigitsOnly()) null else text
        ).map {

            val items = ArrayList<ContentItem>()
            for (item in it!!) {
                items.add(TransactionItem(item))
            }
            items.sortWith(Comparator { p0, p1 ->
                (p1 as TransactionItem).moneyDocument.processingDate.compareTo(
                    (p0 as TransactionItem).moneyDocument.processingDate
                )
            })
            items
        }
    }

    fun createTransaction(username: String, amount: Double, agentUsername: String):
            @NonNull Observable<VIPResponse>? {
        return dataSource.createTransaction(username, amount, agentUsername)
    }

    fun getHandledTransactionsForDate(
        username: String,
        date: Calendar
    ): @NonNull Observable<List<ContentItem>> {
        return dataSource.getHandledRequests(username, "Completed", date)!!
            .map { t ->
                val items = ArrayList<ContentItem>()
                for (item in t!!) {
                    items.add(HandledTransactionItem(item))
                }
                items.sortWith(Comparator { p0, p1 ->
                    (p1 as HandledTransactionItem).moneyDocument.processingDate.compareTo(
                        (p0 as HandledTransactionItem).moneyDocument.processingDate
                    )
                })
                items
            };
    }

    fun payoutTransactionUserConfirmed(): Observable<StatusResponse> {
        return dataSource.payoutTransactionUserConfirmed()
    }
}