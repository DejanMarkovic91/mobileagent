package com.mozzartbet.mobileagent.data.exception

class NetworkError : Exception("Serverska greška, proverite zahtev ili probajte kasnije")