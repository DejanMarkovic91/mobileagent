package com.mozzartbet.mobileagent.data

import com.mozzartbet.mobileagent.data.model.LoggedInUser
import com.mozzartbet.mobileagent.network.NetworkManager
import com.mozzartbet.mobileagent.network.dto.AuthenticationRequest
import com.mozzartbet.mobileagent.network.dto.AuthenticationResponse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.CompletableOnSubscribe
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    private val networkManager: NetworkManager = NetworkManager()

    fun login(username: String, password: String): @NonNull Observable<String>? {
        return Observable.create(ObservableOnSubscribe<String> {
            try {
                val request = AuthenticationRequest(username, password)
                networkManager.service.createSession(request)!!
                    .enqueue(object : Callback<Void?> {
                        override fun onFailure(
                            call: Call<Void?>,
                            error: Throwable
                        ) {
                            it.onError(error)
                        }

                        override fun onResponse(
                            call: Call<Void?>,
                            response: Response<Void?>
                        ) {
                            it.onNext(response.headers()["X-AUTH-TOKEN"])
                        }
                    })
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    fun logout() {
        // TODO: revoke authentication
    }
}

