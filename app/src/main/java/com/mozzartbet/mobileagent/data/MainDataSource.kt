package com.mozzartbet.mobileagent.data

import com.mozzartbet.mobileagent.data.exception.NetworkError
import com.mozzartbet.mobileagent.data.model.LoggedInUser
import com.mozzartbet.mobileagent.network.NetworkManager
import com.mozzartbet.mobileagent.network.dto.*
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

class MainDataSource {

    private lateinit var savedRequest: HandleRequest
    private val networkManager: NetworkManager = NetworkManager()
    private var handledTransactions: List<MoneyDocument> = ArrayList()

    fun getTransactions(): @NonNull Observable<LoggedInUser>? {
        return null
    }

    fun getApplicationConfig(): @NonNull Observable<ApplicationConfigResponse>? {
        return Observable.create(ObservableOnSubscribe<ApplicationConfigResponse> {
            try {
                val response = networkManager.service.getModuleUpdateConfig(
                    "https://user-mobile.mozzartbet.com/user-management/user-config", 13, 0
                )!!
                    .execute().body()
                it.onNext(response)
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    fun getUnassignedRequests(
        username: String,
        requestType: String
    ): @NonNull Observable<List<MoneyDocument>>? {
        return Observable.create(ObservableOnSubscribe<List<MoneyDocument>> {
            try {
                it.onNext(
                    networkManager.service.getRequests(
                        user!!.jwt,
                        username,
                        requestType,
                        "Created"
                    )!!
                        .execute().body() ?: ArrayList()
                )
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    fun getAssignedRequestsToUser(
        user: String,
        requestType: String
    ): @NonNull Observable<List<MoneyDocument>>? {
        return Observable.create(ObservableOnSubscribe<List<MoneyDocument>> {
            try {
                it.onNext(
                    networkManager.service.getRequests(
                        com.mozzartbet.mobileagent.data.user!!.jwt,
                        user,
                        requestType,
                        "Assigned"
                    )!!
                        .execute().body() ?: ArrayList()
                )
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun assignTransaction(username: String, id: Long): @NonNull Observable<StatusResponse>? {
        return Observable.create(ObservableOnSubscribe<StatusResponse> {
            val response = networkManager.service.assignRequest(
                user!!.jwt,
                request = AssignRequest(
                    username,
                    id
                )
            )!!
                .execute().body()
            if (response != null) {
                it.onNext(response)
                it.onComplete()
            } else {
                it.onError(NetworkError())
            }

        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun unassignTransaction(username: String, id: Long): @NonNull Observable<StatusResponse>? {
        return Observable.create(ObservableOnSubscribe<StatusResponse> {
            val response = networkManager.service.unassignRequest(
                user!!.jwt,
                request = AssignRequest(
                    username,
                    id
                )
            )!!
                .execute().body()
            if (response != null) {
                it.onNext(response)
                it.onComplete()
            } else {
                it.onError(NetworkError())
            }

        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun handleTransaction(
        requestType: String,
        id: Long,
        amount: Double,
        code: String?
    ): @NonNull Observable<StatusResponse>? {
        return Observable.create(ObservableOnSubscribe<StatusResponse> {
            if ("Payin" == requestType) {
                val response = networkManager.service.processRequestPayin(
                    user!!.jwt,
                    request = HandleRequest(
                        requestId = id,
                        amount = amount
                    )
                )!!
                    .execute().body()
                it.onNext(response ?: StatusResponse(message = "Error"))
                it.onComplete()
            } else {

                val securityResponse = networkManager.service.checkUserBlocked(
                    user!!.jwt,
                    request = UserStatusRequest(
                        requestId = id,
                        paymentCode = code
                    )
                )!!.execute().body()

                if (securityResponse?.isBlocked == true) {
                    savedRequest = HandleRequest(
                        requestId = id,
                        paymentCode = code
                    )
                    it.onNext(
                        StatusResponse(
                            message = "UserBlockedCheck",
                            userReposnse = securityResponse
                        )
                    )
                    it.onComplete()
                } else {
                    val response = networkManager.service.processRequestPayout(
                        user!!.jwt,
                        request = HandleRequest(
                            requestId = id,
                            paymentCode = code
                        )
                    )!!
                        .execute().body()
                    if (response != null) {
                        it.onNext(response)
                        it.onComplete()
                    } else {
                        it.onError(NetworkError())
                    }
                }
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTransactionById(id: Long): @NonNull Observable<MoneyDocument> {
        return Observable.create(ObservableOnSubscribe<MoneyDocument> {
            try {
                it.onNext(
                    networkManager.service.getTransactionById(user!!.jwt, id)!!
                        .execute().body()
                )
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getUserDetails(
        username: String,
        identityNumber: String?,
        userUsername: String?
    ): Observable<List<MoneyDocument>> {
        return Observable.create(ObservableOnSubscribe<List<MoneyDocument>> {
            try {
                it.onNext(
                    networkManager.service.getLastTransactionsForPlayer(
                        user!!.jwt,
                        identityNumber,
                        userUsername
                    )!!
                        .execute().body() ?: ArrayList()
                )
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun createTransaction(
        username: String,
        amount: Double,
        agentUsername: String
    ): @NonNull Observable<VIPResponse> {
        return Observable.create(ObservableOnSubscribe<VIPResponse> {
            try {
                it.onNext(
                    networkManager.service.createTransactionUnannounced(
                        user!!.jwt,
                        CreateRequest(username, amount, agentUsername)
                    )!!
                        .execute().body() ?: VIPResponse(message = "Error")
                )
                it.onComplete()
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getHandledRequests(
        username: String,
        status: String,
        date: Calendar
    ): Observable<List<MoneyDocument>> {
        return Observable.create(ObservableOnSubscribe<List<MoneyDocument>> {
            try {
                if (handledTransactions.isEmpty()) {
                    handledTransactions = networkManager.service.getRequests(
                        user!!.jwt,
                        username,
                        null,
                        status
                    )!!
                        .execute().body() ?: ArrayList()
                }
                val preparedItems = ArrayList<MoneyDocument>()
                for (item in handledTransactions) {
                    val transDate = Calendar.getInstance()
                    transDate.timeInMillis = item.processingDate
                    if (date.get(Calendar.DAY_OF_YEAR) == transDate.get(Calendar.DAY_OF_YEAR) &&
                        date.get(Calendar.YEAR) == transDate.get(Calendar.YEAR)
                    )
                        preparedItems.add(item)
                }
                it.onNext(preparedItems)
            } catch (e: Throwable) {
                it.onError(e)
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun payoutTransactionUserConfirmed(): Observable<StatusResponse> {
        return Observable.create(ObservableOnSubscribe<StatusResponse> {
            val response = networkManager.service.processRequestPayout(
                user!!.jwt,
                request = savedRequest
            )!!
                .execute().body()
            if (response != null) {
                it.onNext(response)
                it.onComplete()
            } else {
                it.onError(NetworkError())
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}