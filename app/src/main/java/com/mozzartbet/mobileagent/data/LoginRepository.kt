package com.mozzartbet.mobileagent.data

import android.util.Log
import com.mozzartbet.mobileagent.data.model.LoggedInUser
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
var user: LoggedInUser? = null
    internal set

class LoginRepository(val dataSource: LoginDataSource) {

    // in-memory cache of the loggedInUser object

    val isLoggedIn: Boolean
        get() = user != null


    fun logout() {
        user = null
        dataSource.logout()
    }

    fun login(username: String, password: String): @NonNull Observable<String>? {
        return dataSource.login(username, password)
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }
}
